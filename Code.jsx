import React from 'react';
import Embed from 'runkit-embed-react';
import scriptLoader from 'react-async-script-loader';

export const Code = scriptLoader('https://embed.runkit.com')(
  ({ isScriptLoaded, ...rest }) =>
    isScriptLoaded ? (
      <div style={{width: '50vw'}}><Embed {...rest} /></div>
    ) : (
      <p>Loading code example&hellip;</p>
    )
);
