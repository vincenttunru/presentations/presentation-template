import React from 'react';
import { big, highlight } from '@mdx-deck/themes';

const theme = big;

const Provider = props => { 
  const color = (theme.colors) ? theme.colors.primary : 'black';
  const fontFamily = (theme.fonts) ? theme.fonts.body : 'inherit';
  const fontSize = (theme.styles && theme.styles.root) ? theme.styles.root.fontSize[0] : '1.5rem';
  return <>
    <div>
      {props.children}
      <div
        css={{
          position: 'fixed',
          right: 0,
          bottom: 0,
          margin: '3rem 5rem',
          fontSize: fontSize,
          fontFamily: fontFamily,
          color: color,
        }}
      >
        <a
          href="https://twitter.com/VincentTunru"
          title="Me on Twitter"
          style={{color: color}}
        >@VincentTunru</a>
        &nbsp;/&nbsp;
        <a
          href="https://VincentTunru.com"
          title="My blog"
          style={{color: color}}
        >VincentTunru.com</a>
      </div>
    </div>
  </>;
 }

export default {
  ...theme,
  ...highlight,
  Provider,
}
