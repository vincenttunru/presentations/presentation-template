const { fetchDocument } = require('tripledoc');
const { foaf } = require('rdf-namespaces');

fetchDocument('https://www.w3.org/People/Berners-Lee/card')
.then(profileDoc => {
  const profile = profileDoc.getSubject('https://www.w3.org/People/Berners-Lee/card#i');
  const name = profile.getString(foaf.name);
  console.log(name);
});
